import org.junit.Assert;

public class LargestPalindromeProduct {

    public static void main(String[] args) {
        tests(906609);
    }

    public static int getLargestPalindromeProduct() {
        int palindrome = 0;
        for (int i = 100; i < 1000; i++) {
            for (int j = i; j < 1000; j++) {
                int product = i * j;

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(product);

                String revStrProduct = String.valueOf(stringBuilder.reverse());

                if (product == Integer.valueOf(revStrProduct) && product > palindrome) {
                    palindrome = product;
                }
            }
        }
        System.out.println("The largest palindrome made from the product of two 3-digit numbers is: " + palindrome);
        return palindrome;
    }

    public static void tests(int palindromeValue) {
        Assert.assertEquals(getLargestPalindromeProduct(), palindromeValue);
    }
}
